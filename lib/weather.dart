import 'package:flutter/material.dart';

void main() {
  runApp(weather());
}

class weather extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
       backgroundColor: Colors.black,
       body: buildBodyWidget(),
      ),
    );
  }
}

Widget buildBodyWidget(){
  return ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      height: 300,
                      width: double.infinity,
                      child: Image.network(
                        "https://64.media.tumblr.com/4e436a52a79160171e183ef721a03b1f/tumblr_inline_ox0n62zVuV1r287yu_500.gifv",
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      padding: EdgeInsets.only(top: 40),
                      child: Text(
                        "กรุงเทพฯ",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 35, color: Colors.white),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      padding: EdgeInsets.only(top: 90),
                      child: Text(
                        "26°",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 90, color: Colors.white),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      padding: EdgeInsets.only(top: 180),
                      child: Text(
                        "เมฆเป็นส่วนมาก",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      padding: EdgeInsets.only(top: 230),
                      child: Text(
                        "สูงสุด: 30° ต่ำสุด: 19°",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Container(
              height: 200,
              margin: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.transparent,
              ),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(15.0),
                    child: Text('คาดว่ามีเมฆเป็นส่วนมากประมาณ 21:00',
                        style: TextStyle(
                            fontSize: 17,
                            color: Colors.white,
                            fontWeight: FontWeight.bold)),
                  ),
                  Divider(
                    color: Colors.grey,
                    height: 10,
                    indent: 20,
                    endIndent: 20,
                  ),
                  Container(
                    padding: EdgeInsets.all(15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[Hr()],
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 450,
              margin: EdgeInsets.only(right: 20, left: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.transparent,
              ),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(15.0),
                    child: Text('พยากรณ์อากาศ 7 วัน',
                        style: TextStyle(
                            fontSize: 17,
                            color: Colors.white,
                            fontWeight: FontWeight.bold)),
                  ),
                  Divider(
                    color: Colors.grey,
                    height: 10,
                    indent: 20,
                    endIndent: 20,
                  ),
                  Container(
                    padding: EdgeInsets.all(15.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[Week()],
                    ),
                  )
                ],
              ),
            ),
            Divider(
              color: Colors.grey,
              height: 10,
              indent: 20,
              endIndent: 20,
            ),
            Container(
              height: 100,
              margin: const EdgeInsets.only(top: 10, bottom: 10),
              child: UVSun(),
            ),
            Divider(
              color: Colors.grey,
              height: 10,
              indent: 20,
              endIndent: 20,
            ),
            Container(
              height: 100,
              margin: const EdgeInsets.only(top: 10, bottom: 10),
              child: FeelMoist(),
            ),
            Divider(
              color: Colors.grey,
              height: 10,
              indent: 20,
              endIndent: 20,
            ),
          ],
  );
}

FeelMoist(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Feel(),
      Moist(),
    ],
  );
}

Feel(){
  return Column(
    children: <Widget>[
      Text(
        "รู้สึกเหมือน",
        style: TextStyle(fontSize: 13,color: Colors.grey),
        textAlign: TextAlign.center,
      ),
      Text(
        "25°",
        style: TextStyle(fontSize: 30,color: Colors.white),
        textAlign: TextAlign.center,
      ),
      Text(
        "ความชื้นทำให้รู้สกว่าอุ่นขึ้น",
        style: TextStyle(fontSize: 17,color: Colors.white),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

Moist(){
  return Column(
    children: <Widget>[
      Text(
        "ความชื้น",
        style: TextStyle(fontSize: 13,color: Colors.grey),
        textAlign: TextAlign.center,
      ),
      Text(
        "66%",
        style: TextStyle(fontSize: 30,color: Colors.white),
        textAlign: TextAlign.center,
      ),
      Text(
        "จุดน้ำค้างอยู่ที่ 18° ในตอนนี้",
        style: TextStyle(fontSize: 17,color: Colors.white),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

UVSun(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      UV(),
      Sun(),
    ],
  );
}

UV(){
  return Column(
    children: <Widget>[
      Text(
        "ดัชนีรังสี UV",
        style: TextStyle(fontSize: 13,color: Colors.grey),
        textAlign: TextAlign.center,
      ),
      Text(
        "0",
        style: TextStyle(fontSize: 30,color: Colors.white),
        textAlign: TextAlign.center,
      ),
      Text(
        "ต่ำ",
        style: TextStyle(fontSize: 17,color: Colors.white),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

Sun(){
  return Column(
    children: <Widget>[
      Text(
        "พระอาทิตย์ขึ้น",
        style: TextStyle(fontSize: 13,color: Colors.grey),
        textAlign: TextAlign.center,
      ),
      Text(
        "06:42",
        style: TextStyle(fontSize: 30,color: Colors.white),
        textAlign: TextAlign.center,
      ),
      Text(
        "อาทิตย์ตก: 18:02",
        style: TextStyle(fontSize: 17,color: Colors.white),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

Week() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Day1(),
      Day2(),
      Day3(),
      Day4(),
      Day5(),
      Day6(),
      Day7(),
    ],
  );
}

Hr() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Hr1(),
      Hr2(),
      Hr3(),
      Hr4(),
      Hr5(),
      Hr6(),
    ],
  );
}

Widget Day1() {
  return Row(
    children: <Widget>[
      Text(
        "            วันนี้   ",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "  มีเมฆเป็นบางส่วน 19°/30°",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    ],
  );
}

Widget Day2() {
  return Row(
    children: <Widget>[
      Text(
        "            อ.      ",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "  มีเมฆเป็นบางส่วน 20°/31°",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    ],
  );
}

Widget Day3() {
  return Row(
    children: <Widget>[
      Text(
        "            พ.      ",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text(
        "  มีแดดเป็นส่วนใหญ่ 22°/34°",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    ],
  );
}

Widget Day4() {
  return Row(
    children: <Widget>[
      Text(
        "            พฤ.    ",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text(
        "  มีแดดเป็นส่วนใหญ่ 25°/34°",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    ],
  );
}

Widget Day5() {
  return Row(
    children: <Widget>[
      Text(
        "            ศ.      ",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "  มีแเมฆเป็นส่วนมาก 22°/31°",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    ],
  );
}

Widget Day6() {
  return Row(
    children: <Widget>[
      Text(
        "            ส.      ",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "  มีแเมฆเป็นส่วนมาก 22°/31°",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    ],
  );
}

Widget Day7() {
  return Row(
    children: <Widget>[
      Text(
        "            อา.    ",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text(
        "  มีแดดเป็นส่วนใหญ่ 25°/34°",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    ],
  );
}

Widget Hr1() {
  return Column(
    children: <Widget>[
      Text(
        "ตอนนี้",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "26°",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    ],
  );
}

Widget Hr2() {
  return Column(
    children: <Widget>[
      Text(
        "20",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "26°",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    ],
  );
}

Widget Hr3() {
  return Column(
    children: <Widget>[
      Text(
        "21",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "25°",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    ],
  );
}

Widget Hr4() {
  return Column(
    children: <Widget>[
      Text(
        "22",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "24°",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    ],
  );
}

Widget Hr5() {
  return Column(
    children: <Widget>[
      Text(
        "23",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "24°",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    ],
  );
}

Widget Hr6() {
  return Column(
    children: <Widget>[
      Text(
        "24",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "23°",
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    ],
  );
}
